/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useMemo, useRef, useState } from 'react'
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  useColorScheme,
  View,
  Text,
  TouchableOpacity,
  Image
} from 'react-native'

import { Colors } from 'react-native/Libraries/NewAppScreen'

import MapView, {
  Marker,
  Polyline,
  PROVIDER_GOOGLE,
  AnimatedRegion
} from 'react-native-maps'
import MapViewDirections from 'react-native-maps-directions'
import { mapStyle } from './Contants/mapStyle'
import Images from './Images'

const origin = { latitude: 43.644503, longitude: -79.380321 }
const destination = { latitude: 43.64535555, longitude: -79.37550207 }
const destination1 = { latitude: 43.71039991, longitude: -79.25932466 }
const GOOGLE_MAPS_API_KEY = 'AIzaSyDA75qpoFjslAN5XFe-ZnfXn6eeNFhJMig'

let positionTemp = [1, 1]

const App = () => {
  const isDarkMode = useColorScheme() === 'dark'
  const mapRef = useRef(null)
  const [dataPosition, setDataPosition] = useState([])
  const [isTrackingTrain, setSsTrackingTrain] = useState(false)
  const [zoom, setZoom] = useState(13.46)
  const [trainPosition, setTrainPosition] = useState(
    new AnimatedRegion({
      latitude: 43.644503,
      longitude: -79.380321,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421,
      speed: 0
    })
  )
  const [currentTrainPosition, setCurrentTrainPosition] = useState({
    latitude: 43.644503,
    longitude: -79.380321,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
    speed: 0
  })

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
    flex: 1
  }

  const drawTrainLine = useMemo(() => {
    console.log('drawTrainLine')
    return (
      dataPosition &&
      !!dataPosition.length && (
        <MapViewDirections
          origin={{
            latitude: dataPosition[0].latitude,
            longitude: dataPosition[0].longitude
          }}
          destination={{
            latitude: dataPosition[100].latitude,
            longitude: dataPosition[100].longitude
          }}
          mode={'TRANSIT'}
          apikey={GOOGLE_MAPS_API_KEY}
          strokeColor={'red'}
          strokeWidth={4}
        />
      )
    )
  }, [dataPosition])

  const getRotate = (positionTemp) => {
    // TODO
    const x = positionTemp[0],
      y = positionTemp[1]
    if (x > 0) {
      if (y > 0) {
        return `${(y / x) * 10}deg`
      }
    }
    return '180deg'
  }

  const drawTrain = useMemo(() => {
    return (
      <Marker.Animated coordinate={trainPosition}>
        {zoom < 14 ? (
          <View
            style={{
              width: 20,
              height: 20,
              borderRadius: 10,
              backgroundColor: 'red'
            }}
          />
        ) : (
          <Image
            resizeMode={'cover'}
            source={Images.train}
            style={{
              width: 20,
              height: 30,
              transform: [{ rotate: getRotate(positionTemp) }]
            }}
          />
        )}
      </Marker.Animated>
    )
  }, [trainPosition, zoom, positionTemp])

  const goToRegion = (item) => {
    if (item && item.latitude) {
      trainPosition
        .timing({ latitude: item.latitude, longitude: item.longitude }, 500)
        .start()
    }
  }

  const getZoom = async () => {
    const coords = await mapRef?.current.getCamera()
    // console.log({ coords })
    setZoom(coords.zoom)
  }

  const renderSpeed = useMemo(() => {
    return (
      !!currentTrainPosition?.speed && (
        <View
          style={{
            position: 'absolute',
            width: 200,
            height: 100,
            bottom: 0,
            left: 0,
            backgroundColor: '#060F1A',
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >
          <Text style={{ fontSize: 14, fontWeight: 'bold', color: '#fff' }}>
            SPEED
          </Text>
          <Text
            style={{
              color: '#42FF55',
              marginTop: 7,
              fontWeight: 'bold',
              fontSize: 20
            }}
          >
            {currentTrainPosition?.speed.toFixed(2) + ' Km/H'}
          </Text>
        </View>
      )
    )
  }, [currentTrainPosition])

  useEffect(() => {
    fetch('https://portal-map-demo.herokuapp.com/corridorData')
      .then((response) => response.json())
      .then((json) => {
        const data = json.map((item) => ({
          latitude: parseFloat(item[0]),
          longitude: parseFloat(item[1]),
          speed: parseFloat(item[6])
        }))
        setDataPosition(data)
      })
      .catch((error) => {
        console.error(error)
      })
  }, [])

  useEffect(() => {
    if (dataPosition.length) {
      let delay = 0
      for (let i = 0; i < 1000; i++) {
        if (dataPosition[i].latitude) {
          const to = setTimeout(() => {
            positionTemp = [
              dataPosition[i + 1].latitude - dataPosition[i].latitude,
              dataPosition[i + 1].longitude - dataPosition[i].longitude
            ]
            goToRegion(dataPosition[i])
            setCurrentTrainPosition(dataPosition[i])
            clearTimeout(to)
          }, delay + 1000)
          delay += 1000
        }
      }
    }
  }, [dataPosition])

  useEffect(() => {
    isTrackingTrain &&
      mapRef?.current.animateToRegion(
        {
          ...currentTrainPosition,
          latitudeDelta: 0.00922,
          longitudeDelta: 0.0421
        },
        600
      )
  }, [isTrackingTrain, currentTrainPosition])

  return (
    <SafeAreaView style={backgroundStyle}>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <MapView
        ref={mapRef}
        initialRegion={{
          latitude: 43.644503,
          longitude: -79.380321,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421
        }}
        style={StyleSheet.absoluteFill}
        provider={PROVIDER_GOOGLE}
        customMapStyle={mapStyle}
        showsCompass={true}
        onRegionChange={getZoom}
      >
        {drawTrainLine}
        {drawTrain}
      </MapView>
      <TouchableOpacity
        style={styles.btnTracking}
        onPress={() => setSsTrackingTrain(!isTrackingTrain)}
      >
        <Text style={{ color: '#fff' }}>Tracking train</Text>
      </TouchableOpacity>
      {renderSpeed}
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  destination: {
    width: 20,
    height: 20,
    borderRadius: 10,
    backgroundColor: '#C7C7C6'
  },
  btnTracking: {
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    width: 100,
    backgroundColor: 'gray'
  }
})

export default App
